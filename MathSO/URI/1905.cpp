#include <bits/stdc++.h>

using namespace std;
int mapa[6][6];
bool visitado[6][6];

int busca(int x, int y) {
    if (x < 0 || y < 0 || x >= 5 || y >= 5 || visitado[x][y] || mapa[x][y] == 1) {
        return 0;
    }

    if (x == 4 && y == 4) {
        return 1;
    }

    visitado[x][y] = true;

    return busca(x + 1, y) | busca(x - 1, y) | busca(x, y + 1) | busca(x, y - 1);
}

int main() {
    int t;

    scanf(" %d", &t);

    while (t--) {
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                scanf(" %d", mapa[i] + j);
            }   
        }

        if (busca(0, 0)) {
            printf("COPS\n");
        } else {
            printf("ROBBERS\n");
        }

        memset(visitado, 0, sizeof visitado);
    }

    return 0;
}