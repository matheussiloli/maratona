#include <bits/stdc++.h>

using namespace std;

int vet[256];

int main() {
    int t;
    scanf(" %d", &t);

    char c = getchar();
    while (t--) {
        int max = 0;

        while ((c = getchar()) != '\n' && c != EOF) {
            c = tolower(c);
            if (c >= 'a' && c <= 'z') {
                vet[tolower(c)]++;

                if (vet[tolower(c)] > max) {
                    max = vet[tolower(c)];
                }
            }
        }

        for (int i = 'a'; i <= 'z'; i++) {
            if (vet[i] == max) {
                putchar(i);
            }

            vet[i] = 0;
        }
        putchar('\n');
    }

    return 0;
}