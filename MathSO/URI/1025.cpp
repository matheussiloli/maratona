#include <bits/stdc++.h>

using namespace std;
vector<int> vet;

int buscaBin(int ini, int fim, int val) {
    if (ini >= fim) {
        return ini;
    }

    int meio = (ini + fim) / 2;
    if (vet[meio] >= val) {
        return buscaBin(ini, meio, val);
    } else {
        return buscaBin(meio + 1, fim, val);
    }
}

int main() {
    int n, q, t = 1;

    while (scanf(" %d %d", &n, &q) != EOF && n) {
        printf("CASE# %d:\n", t++);
        for (int i = 0; i < n; i++) {
            int aux;
            scanf(" %d", &aux);

            vet.push_back(aux);
        }

        sort(vet.begin(), vet.end());

        for (int i = 0; i < q; i++) {
            int aux;

            scanf(" %d", &aux);

            int idx = buscaBin(0, n, aux);

            if (vet[idx] == aux) {
                printf("%d found at %d\n", aux, idx+1);
            } else {
                printf("%d not found\n", aux);
            }
        }

        vet.clear();
    }


    return 0;
}