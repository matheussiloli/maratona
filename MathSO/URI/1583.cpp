#include <bits/stdc++.h>

using namespace std;
char mapa[60][60];
int n, m;

void busca(int x, int y) {
    if (x < 0 || y < 0 || x >= n || y >= m || mapa[x][y] == 'T' || mapa[x][y] == 'X') {
       return; 
    }

    mapa[x][y] = 'T';

    busca(x + 1, y);
    busca(x - 1, y);
    busca(x, y + 1);
    busca(x, y - 1);
}

int main() {
    int first = 0;
    while (scanf(" %d %d", &n, &m) != EOF) {
        if (first) {
            putchar('\n');
        }
        first = 1;

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                scanf(" %c", mapa[i] + j);
            }
        }

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if (mapa[i][j] == 'T') {
                    busca(i + 1, j);
                    busca(i - 1, j);
                    busca(i, j + 1);
                    busca(i, j - 1);
                }
            }
        }

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                putchar(mapa[i][j]);
            }
            putchar('\n');
        }
    }

    return 0;
}