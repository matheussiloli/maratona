#include <bits/stdc++.h>

using namespace std;
bool visitado[10][10];

int busca(int x, int y, int fx, int fy) {
    // cout << x << " " << y << " " << fx << " " << fy << endl;
    queue<pair<int, pair<int, int>>> pos;

    pos.push(make_pair(x, make_pair(y, 0)));
    while (!pos.empty()) {
        auto v = pos.front();
        pos.pop();

        // cout << v.first << ' ' << v.second.second << endl;
        if (v.first < 0 || v.second.first < 0 || v.first >= 8 || v.second.first >= 8) {
            continue;
        }

        if (v.first == fx && v.second.first == fy) {
            return v.second.second;
        }

        if (!visitado[v.first + 2][v.second.first + 1]) {
            pos.push(make_pair(v.first + 2, make_pair(v.second.first + 1, v.second.second + 1)));
            visitado[v.first + 2][v.second.first + 1] = true;
        }

        if (!visitado[v.first + 2][v.second.first - 1]) {
            pos.push(make_pair(v.first + 2, make_pair(v.second.first - 1, v.second.second + 1)));
            visitado[v.first + 2][v.second.first - 1] = true;
        }

        if (!visitado[v.first - 2][v.second.first + 1]) {
            pos.push(make_pair(v.first - 2, make_pair(v.second.first + 1, v.second.second + 1)));
            visitado[v.first - 2][v.second.first + 1] = true;
        }

        if (!visitado[v.first - 2][v.second.first - 1]) {
            pos.push(make_pair(v.first - 2, make_pair(v.second.first - 1, v.second.second + 1)));
            visitado[v.first - 2][v.second.first - 1] = true;
        }

        if (!visitado[v.first + 1][v.second.first + 2]) {
            pos.push(make_pair(v.first + 1, make_pair(v.second.first + 2, v.second.second + 1)));
            visitado[v.first + 1][v.second.first + 2] = true;
        }

        if (!visitado[v.first + 1][v.second.first - 2]) {
            pos.push(make_pair(v.first + 1, make_pair(v.second.first - 2, v.second.second + 1)));
            visitado[v.first + 1][v.second.first - 2] = true;
        }

        if (!visitado[v.first - 1][v.second.first + 2]) {
            pos.push(make_pair(v.first - 1, make_pair(v.second.first + 2, v.second.second + 1)));
            visitado[v.first - 1][v.second.first + 2] = true;
        }

        if (!visitado[v.first - 1][v.second.first - 2]) {
            pos.push(make_pair(v.first - 1, make_pair(v.second.first - 2, v.second.second + 1)));
            visitado[v.first - 1][v.second.first - 2] = true;
        }
    }

    return -1;
}

int main() {
    char a[3], b[3];

    while (scanf(" %s %s", a, b) != EOF) {
        printf("To get from %s to %s takes %d knight moves.\n", a, b, busca(a[0] - 'a', a[1] - '1', b[0] - 'a', b[1] - '1'));
        memset(visitado, 0, sizeof visitado);
    }
    return 0;
}