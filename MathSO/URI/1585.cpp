#include <bits/stdc++.h>

using namespace std;
int n, vet[112345];
char name[112345][20];

int buscaBin(int ini, int fim) {
    if (ini >= fim) {
        return ini;
    }

    int meio = (ini + fim) / 2;
    long long int fa = 0, fb = 0;
    for (int i = meio; i >= 0; i--) {
        fa += vet[i] * (meio - i + 1);
    }

    for (int i = meio + 1; i < n; i++) {
        fb += vet[i] * (i - meio);
    }

    if (fa == fb) {
        return meio;
    } else if (fa > fb) {
        return buscaBin(ini, meio);
    } else {
        return buscaBin(meio+1, fim);
    }
}

int main() {
    while (scanf(" %d", &n) != EOF && n) {
        for (int i = 0; i < n; i++) {
            scanf(" %s", name[i]);
            vet[i] = 0;

            for (int j = 0; name[i][j]; j++) {
                vet[i] += name[i][j];
            }
        }

        int aux = buscaBin(0, n);
        long long int fa = 0, fb = 0;
        for (int i = aux; i >= 0; i--) {
            fa += vet[i] * (aux - i + 1);
        }

        for (int i = aux + 1; i < n; i++) {
            fb += vet[i] * (i - aux);
        }

        if (fa == fb) {
            printf("%s\n", name[aux]);
        } else {
            printf("Impossibilidade de empate.\n");
        }
    }

    return 0;
}