#include <bits/stdc++.h>

using namespace std;

int main() {
    int t, a, b, c, d, e;

    scanf(" %d %d %d %d %d %d", &t, &a, &b, &c, &d, &e);
    printf("%d\n", (t == a) + (t == b) + (t == c) + (t == d) + (t == e));

    return 0;
}