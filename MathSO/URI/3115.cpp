#include <bits/stdc++.h>

using namespace std;
int val[112345];
vector<pair<int, int>> adj[112345];
bool visitado[112345];

int busca(int x, int max) {
    if (visitado[x]) {
        return max;
    }

    visitado[x] = true;
    for (auto v : adj[x]) {
        busca(v.first, min(max, v.second));
    }

    return val[x] = max;
}

int main() {
    int n;

    scanf(" %d", &n);

    for (int i = 1; i < n; i++) {
        int a, b, c;

        scanf(" %d %d %d", &a, &b, &c);

        adj[a].push_back(make_pair(b, c));
        adj[b].push_back(make_pair(a, c));
    }

    busca(n, INT_MAX);

    printf("%d", val[1]);
    for (int i = 2; i < n; i++) {
        printf(" %d", val[i]);
    }
    putchar('\n');

    return 0;
}